#!/bin/bash

# ----------------------------------------
# loading shell config.
# ----------------------------------------
SHELL_CONFIG_DIR=~/.bashrc.d
if [ ! -d $SHELL_CONFIG_DIR ]; then
  mkdir -p ~/.bashrc.d
fi
cp env_files/{variables,aliases} $SHELL_CONFIG_DIR/

cat >> ~/.bashrc <<EOL
for file in "$SHELL_CONFIG_DIR"/*; do
    [[ -f $file ]] && source "$file"
done
EOL

# ----------------------------------------
# loading fonts.
# ----------------------------------------
SRC_FONT_DIR=fonts
DEST_FONT_DIR=~/.fonts

if [ ! -d $SRC_FONT_DIR ]; then
  mkdir ~/.fonts
fi

unzip $SRC_FONT_DIR/*.zip -C $DEST_FONT_DIR
fc-cache -f

# ----------------------------------------
# loading configs.
# ----------------------------------------
SRC_CONFIG_DIR=config_files
DEST_CONFIG_DIR=~/.configs
cp $SRC_CONFIG_DIR/.tmux.conf $DEST_CONFIG_DIR/   # Tmux config files.
